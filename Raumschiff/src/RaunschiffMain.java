/**
 * 
 */

/**
 * @author a.cevik
 *
 */
public class RaunschiffMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff("IKS Heg'ta",100,100,100,100,1,2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara",100,100,100,100,2,2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var",80,80,50,100,0,5);
		
		klingonen.addLadung(new Ladung("Ferengi Schneckensaft",200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert",200));
		romulaner.addLadung(new Ladung("Borg-Schrott",5));
		romulaner.addLadung(new Ladung("Rote Materie",2));
		romulaner.addLadung(new Ladung("Plasma-Waffe",50));
		vulkanier.addLadung(new Ladung("Forschungssonde",35));
		vulkanier.addLadung(new Ladung("Photonentorpedo",3));
		
		System.out.println(klingonen);
		
	}

}
