import java.util.ArrayList;



public class Raumschiff {



private String Schiffsname="Bork";
private int energieversorgungInProzent =100;
private int schildeInProzent = 100;
private int huelleInProzent =100;
private int lebenserhaltungssystemeInProzent =100;
private int photonentorpedosInAnzahl = 0;
private int androidenAnzahl = 2;
private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

public Raumschiff(String schiffsname, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent,
int lebenserhaltungssystemeInProzent, int photonentorpedosInAnzahl, int androidenAnzahl) {

Schiffsname = schiffsname;
this.energieversorgungInProzent = energieversorgungInProzent;
this.schildeInProzent = schildeInProzent;
this.huelleInProzent = huelleInProzent;
this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
this.photonentorpedosInAnzahl = photonentorpedosInAnzahl;
this.androidenAnzahl = androidenAnzahl;
this.broadcastKommunikator = new ArrayList<String>();
this.ladungsverzeichnis = new ArrayList<Ladung>();
}

public String getSchiffsname() {
return Schiffsname;
}
public void setSchiffsname(String schiffsname) {
Schiffsname = schiffsname;
}
public int getEnergieversorgungInProzent() {
return energieversorgungInProzent;
}
public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
this.energieversorgungInProzent = energieversorgungInProzent;
}
public int getSchildeInProzent() {
return schildeInProzent;
}
public void setSchildeInProzent(int schildeInProzent) {
this.schildeInProzent = schildeInProzent;
}
public int getHuelleInProzent() {
return huelleInProzent;
}
public void setHuelleInProzent(int huelleInProzent) {
this.huelleInProzent = huelleInProzent;
}
public int getLebenserhaltungssystemeInProzent() {
return lebenserhaltungssystemeInProzent;
}
public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
}
public int getPhotonentorpedosInAnzahl() {
return photonentorpedosInAnzahl;
}
public void setPhotonentorpedosInAnzahl(int photonentorpedosInAnzahl) {
this.photonentorpedosInAnzahl = photonentorpedosInAnzahl;
}
public int getAndroidenAnzahl() {
return androidenAnzahl;
}
public void setAndroidenAnzahl(int androidenAnzahl) {
this.androidenAnzahl = androidenAnzahl;
}
public ArrayList<String> getBroadcastKommunikator() {
return broadcastKommunikator;
}
public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
this.broadcastKommunikator = broadcastKommunikator;
}
public ArrayList<Ladung> getLadungsverzeichnis() {
return this.ladungsverzeichnis;
}
public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
this.ladungsverzeichnis = ladungsverzeichnis;


}
public void addLadung(Ladung neueLadung) {
	ladungsverzeichnis.add(neueLadung);
	
}

@Override
public String toString() {
	return "Schiffsname=" + Schiffsname + ", \nenergieversorgungInProzent=" + energieversorgungInProzent
			+ ",  \nschildeInProzent=" + schildeInProzent + ",  \nhuelleInProzent=" + huelleInProzent
			+ ",  \nlebenserhaltungssystemeInProzent=" + lebenserhaltungssystemeInProzent + ",  \nphotonentorpedosInAnzahl="
			+ photonentorpedosInAnzahl + ",  \nandroidenAnzahl=" + androidenAnzahl + ",  \nbroadcastKommunikator="
			+ broadcastKommunikator + ",  \nladungsverzeichnis=" + ladungsverzeichnis;
}







}